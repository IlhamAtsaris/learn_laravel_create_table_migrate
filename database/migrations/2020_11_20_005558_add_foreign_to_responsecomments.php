<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignToResponsecomments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('responsecomments', function (Blueprint $table) {
            $table->foreign('jawaban_id')->references('id')->on('responses');
            $table->foreign('profil_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('responsecomments', function (Blueprint $table) {
            $table->dropForeign(['jawaban_id']);
            $table->dropForeign(['profil_id']);
        });
    }
}
